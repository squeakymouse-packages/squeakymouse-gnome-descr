#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Licensed under the GNU General Public License, version 3.
# See the file http://www.gnu.org/licenses/gpl.txt

from pisi.actionsapi import autotools
from pisi.actionsapi import pisitools
from pisi.actionsapi import get
from pisi.actionsapi import shelltools
from os import system

def setup():
    shelltools.copy("tools/gnome-doc-utils.m4", "m4/")
    system("rm " + get.workDIR()+"/"+get.srcDIR()+"/m4/glib-gettext.m4")
    autotools.autoreconf("-fiv")
    system("export PERL5LIB=/usr/lib/perl5/vendor_perl/5.24.0/x86_64-linux-thread-multi/")
    autotools.configure()

def build():
    autotools.make("-j1")

def install():
    autotools.install()

    pisitools.dodoc("AUTHORS", "ChangeLog", "README", "NEWS")
